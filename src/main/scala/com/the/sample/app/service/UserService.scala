package com.the.sample.app.service

import com.the.sample.app.model.User
import com.the.sample.app.repository.UserRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.{Flux, Mono}

trait UserService {
  def findAll(): Flux[User]

  def findById(id: String): Mono[User]

  def findByEmail(email: String): Mono[User]

  def save(user: User): Unit

  def deleteById(id: String): Unit
}

@Service
class UserServiceImpl(userRepository: UserRepository) extends UserService {
  override def findAll(): Flux[User] =
    userRepository.findAll()

  override def findById(id: String): Mono[User] = userRepository.findById(id)

  override def findByEmail(email: String): Mono[User] = userRepository.findByEmail(email)

  override def save(user: User): Unit = userRepository.save(user)

  override def deleteById(id: String): Unit = userRepository.deleteById(id)
}
