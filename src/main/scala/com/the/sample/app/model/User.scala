package com.the.sample.app.model

import org.springframework.data.cassandra.core.mapping.{PrimaryKey, Table}
import scala.beans.BeanProperty

@Table
class User (@BeanProperty
            var fullName: String,
            @BeanProperty
            var email: String){
  @PrimaryKey
  var id: String = _
  def this() = this(null,null)
}
